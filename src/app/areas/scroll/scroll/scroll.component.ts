import {ChangeDetectorRef, Component} from '@angular/core';
import {ViewportScroller} from "@angular/common";
import {interval} from "rxjs";
import {filter} from "rxjs/operators";

@Component({
  selector: 'app-scroll',
  templateUrl: './scroll.component.html',
  styleUrls: ['./scroll.component.scss']
})
export class ScrollComponent {

  number_array = []
  number_array_length = 40;
  length = 40;
  listener;
  header;
  offset = 1;
  new_content_is_loading = false;

  scroll_height = 0;
  scroll_top = 0;
  offset_height = 0;
  offset_height_custom = 0;
  offset_height_custom_and_scroll_top = 0;

  constructor(private scroll: ViewportScroller,
              private ref: ChangeDetectorRef) {
    for (let j = this.offset; j <= this.number_array_length; j++) {
      this.number_array.push(j);
    }
  }

  on_scroll(i_event: Event) {
    this.scroll_height = Math.round(i_event.target['scrollHeight']);
    this.offset_height = Math.round(document.documentElement.offsetHeight);
    this.offset_height_custom = this.offset_height - 64;
    this.scroll_top = Math.round(i_event.target['scrollTop']);
    this.offset_height_custom_and_scroll_top = Math.round(this.offset_height_custom + this.scroll_top + 88);
    if (this.scroll_height <= this.offset_height_custom_and_scroll_top && this.new_content_is_loading === false) {
      this.new_content_is_loading = true;
      const seconds = interval(2000);
      seconds.pipe(filter(value => value < 1))
        .subscribe(() => {
          this.new_content_is_loading = false;
          this.offset = this.offset + this.length;
          this.number_array_length = this.number_array_length + this.length;
          for (let j = this.offset; j <= this.number_array_length; j++) {
            this.number_array.push(j);
          }
          this.ref.markForCheck();
        });
    }
  }

  scroll_to_top(el: Element) {
    const l_pixel_interval = 20;
    const l_time_interval = 0;
    const seconds = interval(l_time_interval);
    let is_scrolling = true;
    seconds.pipe(filter(value => el.scrollTop > 0 && is_scrolling === true))
      .subscribe(() => {
        const l_scroll_top = el.scrollTop - l_pixel_interval;
        el.scrollTop = l_scroll_top < 0 ? 0 : l_scroll_top;
        is_scrolling = el.scrollTop === 0 ? false : true;
        //console.log(el.scrollTop);
      });
  }

}
