import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ScrollComponent} from './scroll/scroll.component';
import {RouterModule, Routes} from "@angular/router";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {MatProgressSpinnerModule, MatSpinner} from "@angular/material/progress-spinner";

export const route: Routes = [
  {
    path: 'scroll',
    component: ScrollComponent,
    data: {
      animation: 'ScrollPage'
    }
  }
];

@NgModule({
  declarations: [ScrollComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(route),
    MatIconModule,
    MatButtonModule,
    MatProgressSpinnerModule
  ]
})
export class ScrollModule {
}
