import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TouchComponent } from './touch/touch.component';
import {RouterModule, Routes} from "@angular/router";
import {MatButtonModule} from "@angular/material/button";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatCardModule} from "@angular/material/card";
import {SharedModule} from "../../shared/shared.module";

export const route: Routes = [
  {
    path: 'touch',
    component: TouchComponent,
    data: {
      animation: 'TouchPage'
    }
  }
];

@NgModule({
  declarations: [TouchComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(route),
    //
    SharedModule,
    //
    MatButtonModule,
    MatToolbarModule,
    MatCardModule
  ]
})
export class TouchModule {
}
