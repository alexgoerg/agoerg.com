import {Component} from '@angular/core';

@Component({
  selector: 'app-touch',
  templateUrl: './touch.component.html',
  styleUrls: ['./touch.component.scss']
})
export class TouchComponent {

  result = '...';
  result_id = null;
  button_color = 'primary';
  long_touch = false;
  short_touch = false;
  short_touch_time_intervall: number = 3;
  time_out_time = null;
  time_out_long_touch = null;
  time_out_short_touch_start = null;
  time_out_short_touch_end = null;
  time_intervall_short = 100;
  time_intervall_delta = 600 - this.time_intervall_short;
  time_intervall_long = this.time_intervall_short + this.time_intervall_delta;

  constructor() {
  }

  touch_start(i_event: string) {
    // alert(i_event);
    // console.log(i_event);
    this.button_color = 'accent';
    clearTimeout(this.time_out_short_touch_start);
    clearTimeout(this.time_out_short_touch_end);
    this.time_out_short_touch_start = setTimeout(() => {
      this.long_touch = true;
      this.short_touch = true;
      clearTimeout(this.time_out_long_touch);
      this.time_out_long_touch = setTimeout(() => {
        if (this.long_touch) {
          clearTimeout(this.time_out_time);
          this.result = 'long tap';
          this.result_id = 2;
          this.long_touch = false;
          this.short_touch = false;
          this.button_color = 'primary';
        }
      }, this.time_intervall_long);
    }, this.time_intervall_short);
  }

  touch_end(i_event: string) {
    this.long_touch = false;
    clearTimeout(this.time_out_short_touch_end);
    this.time_out_short_touch_end = setTimeout(() => {
      clearTimeout(this.time_out_time);
      this.time_out_time = clearTimeout(this.time_out_long_touch);
      if (this.short_touch) {
        this.result_id = 1;
        // this.result = 'short tap and ...';
        this.result = 'short tap and single tap';
        this.button_color = 'primary';
        // this.switch_result();
      }
      this.short_touch = false;
    }, this.time_intervall_short + 10);
  }

  switch_result() {
    this.result = 'short tap and single tap';
    clearTimeout(this.time_out_time);
    this.time_out_time = setTimeout(() => {
      /*if (this.short_touch_time_intervall === 0) {
        this.short_touch_time_intervall = 3;
      } else {
        this.short_touch_time_intervall -= 1;
        if (this.short_touch_time_intervall === 0) {
          this.result = this.result === 'short tap and ...' ? '... single tap.' : 'short tap and ...';
        }
      }*/
      this.result = this.result === 'short tap and ...' ? '... single tap.' : 'short tap and ...';
      // this.switch_result();
    }, 1500);
  }

  double_tap(i_event: string) {
    setTimeout(() => {
      clearTimeout(this.time_out_short_touch_start);
      clearTimeout(this.time_out_short_touch_end);
      clearTimeout(this.time_out_long_touch);
      clearTimeout(this.time_out_time);
      this.short_touch = false;
      this.result = 'double tap';
      this.result_id = 3;
      this.button_color = 'primary';
    }, 10);
  }

  clear_timeout() {
    clearTimeout(this.time_out_time);
  }

}
