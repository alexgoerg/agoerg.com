import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {ThemeComponent} from './theme/theme.component';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatButtonModule} from "@angular/material/button";
import {MatCardModule} from "@angular/material/card";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatSelectModule} from "@angular/material/select";
import {MatInputModule} from "@angular/material/input";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatRadioModule} from "@angular/material/radio";
import {MatRippleModule} from "@angular/material/core";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {MatIconModule} from "@angular/material/icon";
import {MatDividerModule} from "@angular/material/divider";

const route: Routes = [
  {
    path: 'theme',
    component: ThemeComponent,
    data: {
      animation: 'ThemePage'
    }
  }
];

@NgModule({
  declarations: [ThemeComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(route),
    MatToolbarModule,
    MatButtonModule,
    MatCardModule,
    //
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatCheckboxModule,
    MatRadioModule,
    MatRippleModule,
    MatButtonToggleModule,
    MatSlideToggleModule,
    MatIconModule,
    MatDividerModule
  ]
})
export class ThemeModule {
}
