import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  public theme_name: string = null;
  public theme_type: string = null;

  //
  constructor() {
    if (localStorage.getItem('theme_name')) {
      if (localStorage.getItem('theme_name').includes('theme')) {
        localStorage.clear();
      }
    }

    if (localStorage.getItem('theme_name')) {
      this.theme_name = localStorage.getItem('theme_name');
    } else {
      this.theme_name = 'default';
    }
    if (localStorage.getItem('theme_type')) {
      this.theme_type = localStorage.getItem('theme_type');
    } else {
      this.theme_type = 'light';
    }
    this.change_theme(this.theme_name, this.theme_type);
  }

  change_theme(i_theme_name: string, i_theme_type: string) {
    this.theme_name = i_theme_name;
    this.theme_type = i_theme_type;
    const l_theme_name = i_theme_name + '-' + i_theme_type + '-theme';
    localStorage.setItem('theme_name', i_theme_name);
    localStorage.setItem('theme_type', i_theme_type);
    const l_body = document.getElementById('body');
    l_body.removeAttribute('class');
    l_body.classList.add(l_theme_name);
    l_body.classList.add('mat-typography');
  }
}
