import {Component} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {RestService} from "../../../shared/service/rest/rest.service";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent {

  form: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private http: HttpClient) {
    this.form = this.formBuilder.group({
      avatar: ['']
    });
    /*this.restService.get('backend_techingredient').subscribe((data: any) => {
      console.log(data);
    });*/
  }

  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.form.get('avatar').setValue(file);
    }
  }

  onSubmit() {
    const formData = new FormData();
    formData.append('avatar', this.form.get('avatar').value);

    this.uploadFile(formData).subscribe(
      (data: { code: number, status: string, message: string, session: any }) => {
        // console.log('-----------------------------------');
        console.log(data.session);
        if (data.code === 200) {
          // console.log(data.code);
          this.form.get('avatar').setValue('');
        } else {
          console.log(data.code);
        }
      },
      (err) => {
        console.log(err);
      }
    );

    // this.get_progress();
  }

  get_progress() {
    this.http.get(environment.slim + '/public/index.php/upload').subscribe((data: { code: number, status: string, message: string, session: any }) => {
      console.log(data.session);
      if (data.code === 201) {
        setTimeout(() => {
          console.log('-----------------------------------');
          console.log(data.message);
          this.get_progress();
        }, 10000);
      } else if (data.code === 200) {
        console.log(data.message);
      }
    });
  }

  public uploadFile(data) {
    return this.http.post(environment.slim + '/public/index.php/upload', data);
    // return null;
  }

}
