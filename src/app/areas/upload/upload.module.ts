import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadComponent } from './upload/upload.component';
import {RouterModule, Routes} from "@angular/router";
import {ReactiveFormsModule} from "@angular/forms";

export const route: Routes = [
  {
    path: 'upload',
    component: UploadComponent,
    data: {
      animation: 'UploadPage'
    }
  }
];

@NgModule({
  declarations: [
    UploadComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(route),
    ReactiveFormsModule
  ]
})
export class UploadModule { }
