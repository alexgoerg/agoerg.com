import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {AnimationComponent} from "./animation/animation.component";
import {MySlidePanelComponent} from './animation/my-slide-panel/my-slide-panel.component';
import {MatButtonModule} from "@angular/material/button";
import {SharedModule} from "../../shared/shared.module";
import {MatCardModule} from "@angular/material/card";

const route: Routes = [
  {
    path: 'animation',
    component: AnimationComponent,
    data: {
      animation: 'AnimationPage'
    }
  }
];

@NgModule({
  declarations: [
    AnimationComponent,
    MySlidePanelComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(route),
    //
    SharedModule,
    //
    MatButtonModule,
    MatCardModule
  ]
})
export class AnimationModule {
}
