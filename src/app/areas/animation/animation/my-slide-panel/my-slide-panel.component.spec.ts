import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MySlidePanelComponent } from './my-slide-panel.component';

describe('MySlidePanelComponent', () => {
  let component: MySlidePanelComponent;
  let fixture: ComponentFixture<MySlidePanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MySlidePanelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MySlidePanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
