import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {animate, state, style, transition, trigger} from "@angular/animations";

@Component({
  selector: 'app-my-slide-panel',
  templateUrl: './my-slide-panel.component.html',
  styleUrls: ['./my-slide-panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('slide', [
      state('left', style({ transform: 'translateX(0)' })),
      state('middle', style({ transform: 'translateX(-33.3333%)' })),
      state('right', style({ transform: 'translateX(-66.6666%)' })),
      transition('left => right', animate(600)),
      transition('right => left', animate(600)),
      transition('left => middle', animate(300)),
      transition('middle => left', animate(300)),
      transition('right => middle', animate(300)),
      transition('middle => right', animate(300)),
    ])
  ]
})
export class MySlidePanelComponent implements OnInit {
  @Input() activePane: string = 'left';

  constructor() { }

  ngOnInit(): void {
  }

  test(el) {
    // console.log(el);
  }

  captureStartEvent(i_event) {
    // console.log(i_event);
    // console.log('start');
  }

  captureDoneEvent(i_event) {
    // console.log(i_event)
    // console.log('done');
  }

}
