import {ApplicationRef, Component} from '@angular/core';
import {Title} from "@angular/platform-browser";
import {NavigationEnd, Router, RouterOutlet} from "@angular/router";
import * as kf from "./route-animations";
import {SwUpdate} from "@angular/service-worker";
import {AppConfirmService} from "./shared/component/confirm/app-confirm.service";
import {ThemeService} from "./areas/theme/theme/theme.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    kf.fader
  ]
})
export class AppComponent {
  title = 'A. Görg';
  public active_main_menu: string = null;

  constructor(private titleService: Title,
              appRef: ApplicationRef,
              // swUpdate: SwUpdate,
              private confirmService: AppConfirmService,
              public themeService: ThemeService,
              private router: Router) {
    this.titleService.setTitle(this.title);
    /*swUpdate.available.subscribe(event => {
      this.confirmService.confirm({title: ' ', message: 'Neue Version'})
        .subscribe((result) => {
          if (result) {
            document.location.reload();
          }
        });
    });
*/
    this.router.events.subscribe((data: NavigationEnd) => {
      if (data instanceof NavigationEnd) {
        // console.log(data.url);
        this.active_main_menu = data.url;
      }
    });
  }

  prepareRoute(i_outlet: RouterOutlet) {
    return i_outlet && i_outlet.activatedRouteData && i_outlet.activatedRouteData.animation;
  }


}
