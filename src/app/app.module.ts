import {BrowserModule, HammerModule, HammerGestureConfig, HAMMER_GESTURE_CONFIG} from '@angular/platform-browser';
import {Injectable, NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {SharedModule} from "./shared/shared.module";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatToolbarModule} from "@angular/material/toolbar";
import * as Hammer from "hammerjs";
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {AppConfirmComponent} from "./shared/component/confirm/app-confirm.component";
import {AppConfirmService} from "./shared/component/confirm/app-confirm.service";
import {MatDialogModule} from "@angular/material/dialog";
import {MatButtonModule} from "@angular/material/button";
import {TestComponent} from './areas/test/test.component';
import {HttpClientModule} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class MyHammerConfig extends HammerGestureConfig {
  overrides = <any>{
    'swipe': {direction: Hammer.DIRECTION_ALL}
  }
}

@NgModule({
    declarations: [
        AppComponent,
        //
        AppConfirmComponent,
        TestComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        HammerModule,
        //
        // ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
        //
        AppRoutingModule,
        //
        SharedModule,
        //
        MatToolbarModule,
        MatDialogModule,
        MatButtonModule
    ],
    providers: [
        AppConfirmService,
        {
            provide: HAMMER_GESTURE_CONFIG,
            useClass: MyHammerConfig
        }
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {
}
