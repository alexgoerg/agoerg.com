import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TestComponent } from "./areas/test/test.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: '/theme',
    pathMatch: 'full'
  },
  {
    path: '',
    children: [
      {
        path: '',
        loadChildren: () => import('./areas/touch/touch.module').then(m => m.TouchModule)
      }
    ]
  },
  {
    path: '',
    children: [
      {
        path: '',
        loadChildren: () => import('./areas/scroll/scroll.module').then(m => m.ScrollModule),
      }
    ]
  },
  {
    path: '',
    children: [
      {
        path: '',
        loadChildren: () => import('./areas/theme/theme.module').then(m => m.ThemeModule)
      }
    ]
  },
  {
    path: '',
    children: [
      {
        path: '',
        loadChildren: () => import('./areas/animation/animation.module').then(m => m.AnimationModule)
      }
    ]
  },
  // {
  //   path: '',
  //   children: [
  //     {
  //       path: '',
  //       loadChildren: () => import('./areas/upload/upload.module').then(m => m.UploadModule)
  //     }
  //   ]
  // },
  {
    path: 'test',
    component: TestComponent,
    data: {
      animation: 'TestPage'
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
