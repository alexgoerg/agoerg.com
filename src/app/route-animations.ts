import {trigger, transition, style, query, group, animateChild, animate, keyframes} from '@angular/animations';

export const fader =
  trigger('routeAnimations',
    [
      transition('ScrollPage => AnimationPage, ThemePage => AnimationPage, TouchPage => AnimationPage, UploadPage => AnimationPage', [
        style({
          position: 'relative',
          overflow: 'hidden'
        }),
        query(':enter, :leave', [
          style({
            position: 'absolute',
            top: '0px',
            width: '100%',
            height: '100%'
          })
        ]),
        query(':enter', [
          style({
            top: '100%',
            height: '100%',
            opacity: 0
          })
        ]),
        query(
          ':leave',
          animateChild()
        ),
        group([
          query(
            ':leave .handleOnAnimation',
            [animate('1200ms ease', style({opacity: 0}))],
            {
              optional: true
            }),
          query(':enter', [animate('1200ms ease', style({top: '0%', opacity: 1}))])
        ]),
        query(':enter', animateChild())
      ]),
      //
      //
      //
      //
      //
      //
      transition('AnimationPage => ScrollPage, AnimationPage => ThemePage, AnimationPage => TouchPage, AnimationPage => UploadPage', [
        style({
          position: 'relative',
          overflow: 'hidden'
        }),
        query(':enter, :leave', [
          style({
            position: 'absolute',
            top: '0px',
            width: '100%',
            height: '100%'
          })
        ]),
        query(':leave', [
          style({
            top: '0%',
            height: '100%',
            zIndex: 2
          })
        ]),
        query(':enter .handleOnAnimation', [
          style({
            opacity: 0
          })
        ],
          {
            optional: true
          }),
        query(':leave', animateChild()),
        group([
          query(':enter .handleOnAnimation', [animate('1200ms ease', style({opacity: 1}))],
            {
              optional: true
            }),
          query(':leave', [animate('1200ms ease', style({top: '100%', opacity: 0}))])
        ]),
        query(':enter', animateChild())
      ])
    ]);


/*
export const fader =
  trigger('routeAnimations',
    [
      transition('* => AnimationPage', [
        query(':enter, :leave', [
            style({
              position: 'absolute',
              left: 0,
              width: '100%',
              opacity: 0,
              transform: 'scale(0) translateY(100%)'
            })
          ],
          {
            optional: true
          }),
        query(':enter', [
            animate('600ms ease',
              style({
                opacity: 1,
                transform: 'scale(1) translateY(0)'
              })
            )
          ],
          {
            optional: true
          })
      ])
    ]);
*/
