import {ApplicationRef, Inject, Injectable, PLATFORM_ID} from '@angular/core';
import {fromEvent, Observable} from 'rxjs';
import {debounceTime} from 'rxjs/operators';
import {isPlatformBrowser} from '@angular/common';
// import {SwUpdate} from "@angular/service-worker";

@Injectable({
    providedIn: 'root'
})
export class AppService {

    // swUpdate: SwUpdate;
    appRef: ApplicationRef;

    constructor(@Inject(PLATFORM_ID) private platformId: Object,
                appRef: ApplicationRef,
                // swUpdate: SwUpdate
    ) {
        this.appRef = appRef;
        // this.swUpdate = swUpdate;

    }
}
