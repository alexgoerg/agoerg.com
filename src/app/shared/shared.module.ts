import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MenuComponent} from './component/menu/menu.component';
import {MatSelectModule} from "@angular/material/select";
import {HammerCardComponent} from './component/hammer-card/hammer-card.component';
import {MatCardModule} from "@angular/material/card";
import {MatButtonModule} from "@angular/material/button";
import {HammerTimeDirective} from "./directive/hammer-time/hammer-time.directive";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonToggleModule} from "@angular/material/button-toggle";


@NgModule({
  declarations: [
    MenuComponent,
    HammerCardComponent,
    //
    HammerTimeDirective
  ],
  imports: [
    CommonModule,
    //
    MatSelectModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    MatButtonToggleModule
  ],
  exports: [
    MenuComponent,
    HammerCardComponent,
    HammerTimeDirective
  ]
})
export class SharedModule {
}
