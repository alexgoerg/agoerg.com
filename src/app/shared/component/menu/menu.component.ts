import { Component } from '@angular/core';
import { NavigationEnd, Router } from "@angular/router";
import { filter } from "rxjs/operators";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent {
  url: string = '/';

  constructor(private router: Router) {
    router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    )
      .subscribe((event: NavigationEnd) => {
        this.url = event.urlAfterRedirects;
        // console.log(this.url)
        // console.log(event);
        // console.log(event.url);
        // console.log(event.urlAfterRedirects);
      });
  }

  change_url(i_url) {
    this.router.navigate([i_url]);
  }

}
