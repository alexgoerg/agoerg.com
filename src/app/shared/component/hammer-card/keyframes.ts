import {style} from "@angular/animations";

export const swing = [
  style({transform: 'rotate3d(0, 0, 1, 15deg)', offset: .2}),
  style({transform: 'rotate3d(0, 0, 1, -10deg)', offset: .4}),
  style({transform: 'rotate3d(0, 0, 1, 5deg)', offset: .6}),
  style({transform: 'rotate3d(0, 0, 1, -5deg)', offset: .8}),
  style({transform: 'none', offset: 1})
];

export const slideOutLeft = [
  style({transform: 'translate3d(0, 0, 0)', offset: 0}),
  style({transform: 'translate3d(-100%, 0, 0)', offset: 1})
];

export const zoomOutRight = [
  style({opacity: 1, transform: 'scale3d(0.475, 0.475, 0.475) translate3d(-42px, 0, 0)', offset: .4}),
  style({opacity: 0, transform: 'scale(0.1) translate3d(2000px, 0, 0)', offset: 1})
];

export const wobble = [
  style({transform: 'translate3d(0, 0, 0)', offset: 0}),
  style({transform: 'translate3d(-25%, 0, 0) rotate3d(0, 0, 1, -5deg)', offset: .15}),
  style({transform: 'translate3d(20%, 0, 0) rotate3d(0, 0, 1, 3deg)', offset: .3}),
  style({transform: 'translate3d(-15%, 0, 0) rotate3d(0, 0, 1, -3deg)', offset: .45}),
  style({transform: 'translate3d(10%, 0, 0) rotate3d(0, 0, 1, 2deg)', offset: .6}),
  style({transform: 'translate3d(-5%, 0, 0) rotate3d(0, 0, 1, -1deg)', offset: .75}),
  style({transform: 'translate3d(0, 0, 0)', offset: 1}),
];

export const rotateOutUpRight = [
  style({opacity: 1, offset: 0}),
  style({opacity: 0, transform: 'rotate3d(0, 0, 1, 90deg)', offset: 1}),
];

export const flipOutY = [
  style({transform: 'perspective(400px)', offset: 0}),
  style({transform: 'rotate3d(0, 0, 1, 90deg)', offset: 0.3}),
  style({transform: 'perspective(400px) rotate3d(0, 1, 0, 90deg)', offset: 1}),
];

export const jello = [
  style({transform: 'translate3d(0, 0, 0)', offset: 0}),
  style({transform: 'translate3d(0, 0, 0)', offset: 0.111}),

  style({transform: 'skewX(-12.5deg) skewY(-12.5deg)', offset: 0.222}),
  style({transform: 'skewX(6.25deg) skewY(6.25deg)', offset: 0.333}),
  style({transform: 'skewX(-3.125deg) skewY(-3.125deg)', offset: 0.444}),
  style({transform: 'skewX(1.5625deg) skewY(1.5625deg)', offset: 0.555}),
  style({transform: 'skewX(-0.78125deg) skewY(-0.78125deg)', offset: 0.666}),
  style({transform: 'skewX(0.390625deg) skewY(0.390625deg)', offset: 0.777}),
  style({transform: 'skewX(-0.1953125deg) skewY(-0.1953125deg)', offset: 0.888}),

  style({transform: 'translate3d(0, 0, 0)', offset: 1}),

];
