import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {Component, Inject} from '@angular/core';
import {ThemeService} from "../../../areas/theme/theme/theme.service";

@Component({
    selector: 'app-confirm',
    template: `<div mat-dialog-content>{{ data.message }}</div>
    <div mat-dialog-actions>
    <h1 matDialogTitle class="mb-05">{{ data.title }}</h1>
        <button
                type="button"
                mat-raised-button
                color="primary"
                (click)="dialogRef.close(true)">OK
        </button>
    </div>`,
})
export class AppConfirmComponent {
    constructor(
        public dialogRef: MatDialogRef<AppConfirmComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        public themeService: ThemeService
    ) {
    }
}
