import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {AppConfirmService} from "../../component/confirm/app-confirm.service";

@Injectable({
    providedIn: 'root'
})
export class ErrorService {

    constructor(private router: Router,
                private confirmService: AppConfirmService
    ) {
    }

    public handleError() {
        this.confirmService.confirm({title: ' ', message: 'Es ist ein unerwarteter Fehler aufgetreten. Seite bitte neu laden und dann anmelden.'})
            .subscribe((result) => {
                if (result) {
                    const l_translation_id = localStorage.getItem('language_id');
                    localStorage.clear();
                    localStorage.setItem('language_id', l_translation_id);
                    // this.router.navigate(['/']);
                    window.location.href = '/';
                }
            });
        return null;
    }

}
