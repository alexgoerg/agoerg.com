import {Inject, Injectable, PLATFORM_ID} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {timeout} from 'rxjs/operators';
import {catchError} from 'rxjs/operators';
import {ErrorService} from '../error/error.service';
import {isPlatformBrowser} from '@angular/common';
import {environment} from '../../../../environments/environment';
import {AppConfirmService} from "../../component/confirm/app-confirm.service";

@Injectable({
  providedIn: 'root'
})
export class RestService {

  getTimer = 100000;
  postTimer = 50000;
  putTimer = 50000;
  deleteTimer = 50000;
  apiUrl = environment.slim + '/public/index.php/api/';
  token = '';

  constructor(private http: HttpClient,
              private errorService: ErrorService,
              @Inject(PLATFORM_ID) private platformId: Object,
              private confirmService: AppConfirmService) {
  }

  get(url, iHeader: any | string = '', token = this.token, timeOut = this.getTimer) {
    if (isPlatformBrowser(this.platformId)) {
      token = localStorage.getItem('bearer_token');
    }
    // token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwYXNzd29yZF9lbmNyeXB0ZWQiOiIwMUY0NjZGNzFGIiwidXNlcl9pZCI6MTIsInJvbGVfYmFja2VuZCI6ImFkbWluIiwiaXNfYWN0aXZlIjoxLCJsYW5ndWFnZV9pZCI6MSwiZW5lcmd5X3VuaXQiOiJrY2FsIiwibGFzdF92aXNpdGVkX3BhZ2UiOiJcL3VzZXIiLCJzZXNzaW9uX2lkIjoiM0pMc2s5c3NrcnZQWW9qQ0p4VnEiLCJ1c2VyX2hhc19kZXNjcmlwdGlvbiI6MiwiZnJvbnRfb3JfYmFja2VuZCI6MSwidW5peHRpbWVzdGFtcCI6MTYxNjUxNzQ3OX0.kCJtMTlfalpJDr2oaFn8lndj8imACHBGr3cJfpFOJDY';
    let headers = new HttpHeaders()
      .set('Authorization', 'Bearer ' + token)
    if (iHeader) {
      headers = new HttpHeaders()
        .set('Authorization', 'Bearer ' + token)
        .append('Content-Type', 'application/json; charset=utf-8')
        .append('x-nutrition-data', JSON.stringify(iHeader));
    }

    return this.http
      .get(this.apiUrl + url + '?ngsw-bypass=true', {headers})
      .pipe(
        timeout(timeOut),
        catchError(err => {
          return this.errorService.handleError();
          // return null;
        })
      );

  }


  post(url, model, token = this.token, timeOut = this.postTimer): Observable<any> {
    if (isPlatformBrowser(this.platformId)) {
      token = localStorage.getItem('bearer_token');
    }
    // token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwYXNzd29yZF9lbmNyeXB0ZWQiOiIwMUY0NjZGNzFGIiwidXNlcl9pZCI6MTIsInJvbGVfYmFja2VuZCI6ImFkbWluIiwiaXNfYWN0aXZlIjoxLCJsYW5ndWFnZV9pZCI6MSwiZW5lcmd5X3VuaXQiOiJrY2FsIiwibGFzdF92aXNpdGVkX3BhZ2UiOiJcL3VzZXIiLCJzZXNzaW9uX2lkIjoiM0pMc2s5c3NrcnZQWW9qQ0p4VnEiLCJ1c2VyX2hhc19kZXNjcmlwdGlvbiI6MiwiZnJvbnRfb3JfYmFja2VuZCI6MSwidW5peHRpbWVzdGFtcCI6MTYxNjUxNzQ3OX0.kCJtMTlfalpJDr2oaFn8lndj8imACHBGr3cJfpFOJDY';
    const headers = new HttpHeaders()
      .set('Authorization', 'Bearer ' + token);
    return this.http
      .post(this.apiUrl + url, model, {headers})
      .pipe(
        timeout(timeOut),
        catchError(err => {
          return this.errorService.handleError();
          // return null;
        })
      );
  }

  put(elements, model, token = this.token, timeOut = this.putTimer): Observable<any> {
    if (isPlatformBrowser(this.platformId)) {
      token = localStorage.getItem('bearer_token');
    }
    const headers = new HttpHeaders()
      .set('Authorization', 'Bearer ' + token);
    return this.http
      .put(this.apiUrl + elements, model, {headers})
      .pipe(
        timeout(timeOut),
        catchError(err => {
          return this.errorService.handleError();
          // return null;
        })
      );
  }

  delete(url, token = this.token, timeOut = this.deleteTimer): Observable<any> {
    if (isPlatformBrowser(this.platformId)) {
      token = localStorage.getItem('bearer_token');
    }
    const headers = new HttpHeaders()
      .set('Authorization', 'Bearer ' + token);
    return this.http
      .delete(this.apiUrl + url, {headers})
      .pipe(
        timeout(timeOut),
        catchError(err => {
          return this.errorService.handleError();
          // return null;
        })
      );
  }

  handleExceptions(i_status_code, i_message) {
    this.confirmService.confirm({title: ' ', message: i_message})
      .subscribe((result) => {
        if (result) {
          const l_translation_id = localStorage.getItem('language_id');
          localStorage.clear();
          localStorage.setItem('language_id', l_translation_id);
          // this.router.navigate(['/']);
          window.location.href = '/';
        }
      });
  }

}
